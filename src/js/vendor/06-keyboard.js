;(function () {
  'use strict'

  document.addEventListener('keydown', function (event) {
    if (event.target.nodeName === 'TEXTAREA' ||
        event.target.nodeName === 'SELECT'
    ) {
      return
    }

    var focused = document.activeElement
    if (!focused || focused === document.body) {
      focused = null
    } else {
      focused = document.querySelector(':focus')
    }
    if (focused) {
      return
    }

    var plain = !(event.altKey || event.ctrlKey || event.metaKey)
    var keyCode = event.code
    var link

    switch (keyCode) {
      case 'KeyT':
        if (document.activeElement ===
          document.getElementById('search-input')) {
          return
        }
        window.scrollTo(0, 0)
        event.preventDefault()
        break

      case 'KeyB':
        if (document.activeElement ===
          document.getElementById('search-input')) {
          return
        }
        window.scrollTo(0, document.body.scrollHeight)
        event.preventDefault()
        break

      case 'ArrowLeft':
        if (plain) {
          link = document.querySelector('link[rel="prev"]')
        }
        break

      case 'ArrowRight':
        if (plain) {
          link = document.querySelector('link[rel="next"]')
        }
        break

      case 'Slash':
        if (plain) {
          document.getElementById('search-input').focus()
          event.preventDefault()
        }
        break

      case 'KeyF':
        if (event.altKey) {
          document.getElementById('search-input').focus()
          event.preventDefault()
        }
        break

      case 'Escape':
        document.getElementById('search-input').blur()
        break
    }

    if (link) {
      window.location = link.getAttribute('href')
    }
  }, false)
})()
